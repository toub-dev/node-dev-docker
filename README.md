# Node dev docker

Docker container with tools and utilities installed for development.

Git repository: <https://gitlab.com/toub-dev/node-dev-docker>
Docker repository: <https://hub.docker.com/repository/docker/toubdev/node-dev-docker>

## Description

Base image: <https://hub.docker.com/_/node/>

Additional tools:

- via APT: sudo bash wget vim git bash-completion xsel rubygems build-essential ruby-dev
- via GEM: fastlane
- npm completion
- sexy bash prompt: <https://github.com/twolfson/sexy-bash-prompt>
- ls colors: <https://github.com/trapd00r/LS_COLORS>
- npm aliases: (ns = "npm start", ni = "npm install")

## Build

```bash
DOCKER_IMAGE_NAME=toubdev/node-dev-docker
DOCKER_IMAGE_TAG=16.20.1-buster
DOCKER_BUILDKIT=1

DOCKER_BUILDKIT=$DOCKER_BUILDKIT docker build -t $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG .
# docker scan --accept-license  --file Dockerfile $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_TAG

```

## Run standalone

```bash
export DOCKER_HOST_IP=$(docker network inspect --format='{{range .IPAM.Config}}{{.Gateway}}{{end}}' bridge)

docker run --rm -it \
  -v $(pwd):/app:delegated \
  -v $HOME/.ssh:/home/node/.ssh:ro \
  -v $HOME/.gitconfig:/home/node/.gitconfig:ro \
  -v $HOME/.npm:/home/node/.npm:delegated \
  toubdev/node-dev-docker:16.20.1-buster bash
  bash
```

## Run with docker-compose

```yaml
services:
  my_dev_app:
    container_name: my_dev_app
    image: docker.io/toubdev/node-dev-docker:16.20.1-buster
    command: tail -f /dev/null
    working_dir: /app
    volumes:
      - $APP_DIR:/app:delegated
      - $HOME/.ssh:/home/node/.ssh:ro
      - $HOME/.gitconfig:/home/node/.gitconfig:ro
      - $HOME/.npm:/home/node/.npm:delegated
    environment:
      TZ: Europe/Paris
```

```bash
APP_DIR=$(pwd) docker-compose up
```

## Add new version

Search & replace version, then:

```bash
git commit -am "node:16.20.1-buster" && git tag 16.20.1-buster && git push && git tag
```
